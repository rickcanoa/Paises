﻿using System;
using System.Collections.Generic;
using Paises.ViewModel;
using Xamarin.Forms;

namespace Paises
{
    public partial class PaisesPage : ContentPage
    {
        public PaisesPage()
        {
            this.BindingContext = 
                new PaisesViewModel();
            InitializeComponent();
        }
    }
}
